Vue.component('message', {
    props: ['title', 'body'],
    data: function () {
        return {
            isVisible: true
        }
    },
    template: `
        <section v-show="isVisible" class="section">
            <div class="container">
                <article class="message">
                    <div class="message-header">
                        <p>{{ title }}</p>
                        <button class="delete" aria-label="delete" @click="hideMessage"></button>
                    </div>
                    <div class="message-body">
                        {{ body }}
                    </div>
                </article>
            </div>
        </section>
    `,
    methods: {
        hideMessage: function () {
            this.isVisible = false;
        }
    }
});

new Vue({
    el: '#root'
});
